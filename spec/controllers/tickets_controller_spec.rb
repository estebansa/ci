require 'rails_helper'

RSpec.describe TicketsController, type: :controller do

  let(:user) {
    FactoryGirl.create(:user)
  }

  let(:ticket){
    FactoryGirl.create(:ticket, user_id: user.id)
  }

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:ticket)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:ticket, title: nil, description: nil, status: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      sign_in user
      get :index
      expect(response).to have_http_status(200)
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      sign_in user
      get :new
      expect(response).to be_successful
    end
  end

  # describe "GET #edit" do
  #   it "returns a success response" do
  #     sign_in user
  #     get :edit, params: { id: ticket.to_param }
  #     expect(response).to be_successful
  #   end
  # end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Ticket" do
        sign_in user
        expect {
          Ticket.create(valid_attributes)
        }.to change(Ticket, :count).by(1)
      end

      it "redirects to the created ticket" do
        sign_in user
        post :create, params: { ticket: valid_attributes }
        expect(response).to be_successful
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        sign_in user
        post :create, params: {ticket: invalid_attributes}, session: session
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested ticket" do
        sign_in user
        put :update, params: { id: ticket.to_param, ticket: FactoryGirl.attributes_for(:ticket, title: 'Test') }
        ticket.reload
        expect(ticket.title).to eq('Test')
      end

      it "redirects to the ticket" do
        sign_in user
        put :update, params: {id: ticket.to_param, ticket: valid_attributes}, session: session
        expect(response).to redirect_to(admin_tickets_path)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        sign_in user
        put :update, params: { id: ticket.to_param, ticket: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested ticket" do
      ticket = Ticket.create! valid_attributes
      sign_in user
      expect {
        delete :destroy, params: {id: ticket.to_param}, session: session
      }.to change(Ticket, :count).by(-1)
    end

    it "redirects to the tickets list" do
      sign_in user
      ticket = Ticket.create! valid_attributes
      delete :destroy, params: {id: ticket.to_param}, session: session
      expect(response).to redirect_to(tickets_url)
    end
  end

end
