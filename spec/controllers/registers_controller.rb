require 'rails_helper'

RSpec.describe RegistersController, type: :controller do
  before do
    @user = FactoryGirl.create(:user)
    @register = FactoryGirl.create(:register, monto: 123)
  end

  context 'when user is logged in' do

    describe 'index action' do
      it 'responds succesfully' do
        sign_in @user
        get :index
        expect(response).to have_http_status(200)
      end
    end

    describe 'create action' do
      it 'adds a register' do
        register_params = FactoryGirl.attributes_for(:register, user_id: @user.id)
        sign_in @user
        expect {
          @user.registers.create(register_params)
        }.to change(@user.registers, :count).by(1)
      end
    end

    describe 'update action' do
      it 'updates a register' do
        register_params = FactoryGirl.attributes_for(:register, user_id: @user.id, monto: 123)
        sign_in @user
        patch :update, params: { id: @register.id, register: register_params }
        expect(@register.reload.monto).to eq 123
      end

      it 'does not update the project' do
        other_user = FactoryGirl.create(:user)
        @register = FactoryGirl.create(:register, user_id: other_user.id, monto: 123)
        register_params = FactoryGirl.attributes_for(:register, user_id: @user.id, monto: 987)
        patch :update, params: { id: @register.id, register: register_params }
        expect(@register.reload.monto).to eq 123
      end
    end

  end

  context 'when user is not logged in' do

    describe 'index action' do
      it 'validates authentication succesfully' do
        get :index
        expect(response).to have_http_status(302)
        expect(response).to redirect_to "/users/sign_in"
      end
    end

    describe 'create action' do
      it 'adds a register' do
        register_params = FactoryGirl.attributes_for(:register, user_id: @user.id)
        post :create, params: { register: register_params }
        expect(response).to have_http_status "302"
      end
    end

  end
end