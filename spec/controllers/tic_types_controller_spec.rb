require 'rails_helper'

RSpec.describe TicTypesController, type: :controller do

  let(:tic_type) {
    FactoryGirl.create(:tic_type)
  }

  let(:user) {
    FactoryGirl.create(:user)
  }

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:tic_type)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:tic_type, name: nil)
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      sign_in user
      get :index
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      sign_in user
      get :new
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      sign_in user
      get :edit, params: { id: tic_type.to_param }
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new TicType" do
        sign_in user
        expect {
          FactoryGirl.create(:tic_type)
        }.to change(TicType, :count).by(1)
      end

      it "redirects to the created tic_type" do
        sign_in user
        post :create, params: { tic_type: valid_attributes }
        expect(response).to be_successful
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        sign_in user
        post :create, params: { tic_type: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:tic_type, name: 'Test')
      }

      it "updates the requested tic_type" do
        sign_in user
        put :update, params: {id: tic_type.to_param, tic_type: new_attributes}
        tic_type.reload
        expect(tic_type.name).to eq('Test')
      end

      it "redirects to the tic_type" do
        sign_in user
        put :update, params: {id: tic_type.to_param, tic_type: valid_attributes}
        expect(response).to redirect_to(tic_type)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        sign_in user
        put :update, params: { id: tic_type.to_param, tic_type: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested tic_type" do
      tic_type = FactoryGirl.create(:tic_type)
      sign_in user
      expect {
        delete :destroy, params: { id: tic_type.id }
      }.to change(TicType, :count).by(-1)
    end

    it "redirects to the tic_types list" do
      sign_in user
      delete :destroy, params: { id: tic_type.to_param }
      expect(response).to redirect_to(tic_types_url)
    end
  end

end
