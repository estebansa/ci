require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  before do
    @user = FactoryGirl.create(:user)
  end

  context 'when user is logged in' do

    describe 'Index action' do
      it 'get' do
        sign_in @user
        get :index
        expect(response).to have_http_status(200)
      end
    end

    describe 'About action' do
      it 'get' do
        sign_in @user
        get :about
        expect(response).to have_http_status(200)
      end
    end

    describe 'Contact action' do
      it 'get' do
        sign_in @user
        get :contact
        expect(response).to have_http_status(200)
      end
    end

  end

  context 'when user is not logged in' do

    describe 'Index action' do
      it 'get' do
        get :index
        expect(response).to have_http_status(200)
      end
    end

    describe 'About action' do
      it 'get' do
        get :about
        expect(response).to have_http_status(200)
      end
    end

    describe 'Contact action' do
      it 'get' do
        get :contact
        expect(response).to have_http_status(200)
      end
    end
  end

end