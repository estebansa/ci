require 'rails_helper'

RSpec.describe "Contact", type: :feature do
  scenario "User visit contact page" do
    visit "/About"
    expect(page).to have_content("Esteban Saldarriaga")
    expect(page).to have_content("EIDOS")
  end
end