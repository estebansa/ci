require 'rails_helper'

RSpec.describe "Index", type: :feature do
  scenario "User visit index page" do
    FactoryGirl.create(:register)
    visit root_path
    expect(page).to have_content(Register.count)
  end
end