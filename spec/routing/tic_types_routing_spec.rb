require "rails_helper"

RSpec.describe TicTypesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/tic_types").to route_to("tic_types#index")
    end

    it "routes to #new" do
      expect(:get => "/tic_types/new").to route_to("tic_types#new")
    end

    it "routes to #show" do
      expect(:get => "/tic_types/1").to route_to("tic_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/tic_types/1/edit").to route_to("tic_types#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/tic_types").to route_to("tic_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/tic_types/1").to route_to("tic_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/tic_types/1").to route_to("tic_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/tic_types/1").to route_to("tic_types#destroy", :id => "1")
    end
  end
end
