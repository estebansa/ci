FactoryGirl.define do
  factory :register do
    association :user
    monto { Faker::Number.number(10) }
    medida { Faker::Number.number(2) }
    descripcion { Faker::Lorem.words(3) }
    fecha { Faker::Date.between(2.days.ago, Date.today) }
    costos_adicionales { Faker::Number.number(3) }
  end
end