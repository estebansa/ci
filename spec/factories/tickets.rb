FactoryGirl.define do
  factory :ticket do
    association :tic_type
    title { Faker::Lorem.word() }
    description { Faker::Lorem.words(5) }
    status :solved
  end
end
