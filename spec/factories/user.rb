FactoryGirl.define do
	factory :user do
		email { Faker::Internet.email }
		permissions_level 1
		password 'passwordtest'
	end 
end