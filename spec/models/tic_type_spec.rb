require 'rails_helper'

RSpec.describe TicType, type: :model do
  it "TicketType is valid" do
    tic_type = FactoryGirl.create(:tic_type)
    expect(tic_type.valid?).to eq(true)
  end

  it "TicketType is invalid" do
    tic_type = TicType.new
    tic_type.valid?
    expect(tic_type.errors[:name]).to include("no puede estar en blanco")
  end

  it "TicketType associations" do
    tic_type = FactoryGirl.create(:tic_type)
    2.times {
      FactoryGirl.create(:ticket, tic_type_id: tic_type.id)
    }
    expect(tic_type).to have_many(:tickets)
  end
end
