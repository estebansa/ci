require 'rails_helper'

RSpec.describe User, type: :model do
  it "User's model is valid" do
  	user = FactoryGirl.create(:user)
  	expect(user.valid?).to eq(true)
  end

  it "User's model is invalid" do
    user = User.new
    user.valid?
    expect(user.errors[:email]).to include("no puede estar en blanco")
    expect(user.errors[:password]).to include("no puede estar en blanco")
  end

  it "User creates a register" do
    user = FactoryGirl.create(:user)
    register = FactoryGirl.create(:register, user_id: user.id)

    expect(register.user_id).to eq(user.id)
    expect{
      register = FactoryGirl.create(:register, user_id: user.id)
    }.to change(user.registers, :count).by(1)
  end

  it "User destroys a register" do
    user = FactoryGirl.create(:user)
    register = FactoryGirl.create(:register, user_id: user.id)

    expect(register.user_id).to eq(user.id)
    expect{
      user.registers.first.delete
    }.to change(user.registers, :count).by(-1)
  end

  it "User destroys a register" do
    user = FactoryGirl.create(:user)
    register = FactoryGirl.create(:register, user_id: user.id)
    expect(register.user_id).to eq(user.id)
    user.registers.first.update(monto: 100)
    expect(user.registers.first.monto).to eq(100)
  end
end
