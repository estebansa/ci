require 'rails_helper'

RSpec.describe Ticket, type: :model do
  it "Is a valid ticket" do
    ticket = FactoryGirl.create(:ticket)
    expect(ticket.valid?).to eq(true)
  end

  it "Is a invalid ticket" do
    ticket = Ticket.new
    ticket.valid?
    expect(ticket.errors[:title]).to include("no puede estar en blanco")
    expect(ticket.errors[:description]).to include("no puede estar en blanco")
  end

  it "Ticket's associations" do
    ticket = FactoryGirl.create(:ticket)
    expect(ticket).to belong_to(:tic_type)
  end
end
