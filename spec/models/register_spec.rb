require 'rails_helper'

RSpec.describe Register, type: :model do
  it "Register's model is valid" do
    register = FactoryGirl.create(:register)
    expect(register.valid?).to eq(true)
  end

  it "Register's model is invalid" do
    register = Register.new
    register.valid?
    expect(register.errors[:monto]).to include("no puede estar en blanco")
    expect(register.errors[:medida]).to include("no puede estar en blanco")
    expect(register.errors[:fecha]).to include("no puede estar en blanco")
    expect(register.errors[:descripcion]).to include("no puede estar en blanco")
  end

  it "Register's associations" do
    register = FactoryGirl.create(:register)
    expect(register).to belong_to(:user)
    expect(register).to belong_to(:category)
  end
end