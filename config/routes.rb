Rails.application.routes.draw do

  resources :tickets
  resources :tic_types
  devise_for :controllers
  namespace :admin do
    resources :users
    resources :categories
    resources :registers

    resources :tickets do
      member do
        patch :change_status
      end
    end

    root to: "users#index"
  end

  resources :categories
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }

  resources :registers do
    collection do
      match 'search' => 'registers#compare', via: [:get, :post], as: :search
    end
  end
  # resources :registers , only: [:create]

  root 'welcome#index'
  get "/About", to: "welcome#about"
  get "/Contact", to: "welcome#contact"
  get "/Index", to: "welcome#index"
  get "/Compare", to: "registers#compare"
  get "/Console", to: "welcome#console"
  get "/mobile", to: "welcome#mobile"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
