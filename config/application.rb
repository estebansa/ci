require_relative 'boot'

require 'rails/all'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SpendoRails
  class Application < Rails::Application

    Rails.application.config.active_record.sqlite3.represent_boolean_as_integer = true
    config.i18n.default_locale = :es
  end
end
