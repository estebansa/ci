class AddColumCantidadMedidaToRegisters < ActiveRecord::Migration[5.2]
	def self.up
		remove_column :registers, :cantidad_medida
	end
	def self.down
		add_column :registers, :consumo, :integer, default: 0
	end
end
