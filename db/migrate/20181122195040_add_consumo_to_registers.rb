class AddConsumoToRegisters < ActiveRecord::Migration[5.2]
  def change
  	add_column :registers, :consumo, :integer, default: 0
  end
end
