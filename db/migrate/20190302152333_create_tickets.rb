class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :description
      t.integer :status, default: 0
      t.string :screenshot
      t.references :tic_type, foreign_key: true

      t.timestamps
    end
  end
end
