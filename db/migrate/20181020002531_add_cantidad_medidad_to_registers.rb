class AddCantidadMedidadToRegisters < ActiveRecord::Migration[5.2]
  def change
    add_column :registers, :cantidad_medida, :integer, default: 0
  end
end
