class CreateRegisters < ActiveRecord::Migration[5.2]
  def change
    create_table :registers do |t|
      t.integer :monto
      t.string :tipo
      t.date :fecha
      t.text :descripcion
      t.string :medida
      t.integer :costos_adicionales

      t.timestamps
    end
  end
end
