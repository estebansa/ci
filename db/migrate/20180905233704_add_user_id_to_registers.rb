class AddUserIdToRegisters < ActiveRecord::Migration[5.2]
  def change
    add_reference :registers, :user, foreign_key: true
  end
end
