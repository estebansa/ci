class AddCategoryIdToRegisters < ActiveRecord::Migration[5.2]
  def change
    add_reference :registers, :category, foreign_key: true
  end
end
