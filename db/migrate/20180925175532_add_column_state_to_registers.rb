class AddColumnStateToRegisters < ActiveRecord::Migration[5.2]
  def change
  	add_column :registers, :state, :string, default: "active"
  end
end
