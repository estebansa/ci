class AddAttachmentScreenshotToTickets < ActiveRecord::Migration[5.2]
  def self.up
    change_table :tickets do |t|
      t.attachment :screenshot
    end
  end

  def self.down
    remove_attachment :tickets, :screenshot
  end
end
