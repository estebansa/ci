class AddPermissionLevelToUsers < ActiveRecord::Migration[5.2]
	def self.up
		add_column :users, :permissions_level, :integer, default: 1		
	end
end
