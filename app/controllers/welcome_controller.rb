class WelcomeController < ApplicationController
  def index
  	@users = User.all.count
  	@registers = Register.all.count
  end

  def about
  end

  def contact
  end

  def console
  end

  def mobile
  end

end
