class TicTypesController < ApplicationController
  before_action :set_tic_type, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @tic_types = TicType.all
  end

  # GET /tic_types/new
  def new
    @tic_type = TicType.new
  end

  # GET /tic_types/1/edit
  def edit
  end

  # POST /tic_types
  # POST /tic_types.json
  def create
    @tic_type = TicType.new(tic_type_params)

    respond_to do |format|
      if @tic_type.save
        format.html { redirect_to @tic_type, notice: 'Tic type was successfully created.' }
        format.json { render :show, status: :created, location: @tic_type }
      else
        format.html { render :new }
        format.json { render json: @tic_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tic_types/1
  # PATCH/PUT /tic_types/1.json
  def update
    respond_to do |format|
      if @tic_type.update(tic_type_params)
        format.html { redirect_to @tic_type, notice: 'Tic type was successfully updated.' }
        format.json { render :show, status: :ok, location: @tic_type }
      else
        format.html { render :edit }
        format.json { render json: @tic_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tic_types/1
  # DELETE /tic_types/1.json
  def destroy
    @tic_type.destroy
    respond_to do |format|
      format.html { redirect_to tic_types_url, notice: 'Tic type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tic_type
      @tic_type = TicType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tic_type_params
      params.require(:tic_type).permit(:name)
    end
end
