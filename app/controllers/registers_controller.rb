class RegistersController < ApplicationController
	before_action :authenticate_user!
	before_action :set_register, except: [:index,:new,:create,:compare]

	def index
		@search = current_user.registers.search(params[:q])
		@registers = @search.result(distinct: true).paginate(page: params[:page], per_page: 5).ultimos.activos
		@search.build_condition if @search.conditions.empty?

		respond_to do |format|
			format.html
			format.json
			format.pdf {render template: 'registers/reporte', pdf: 'Reporte', layout: 'pdf.html'}
		end


	end
		# if params[:search]
		# 	@registers = current_user.registers.where('monto LIKE ?', "%#{params[:search]}%").paginate(page: params[:page], per_page: 10).publicados.ultimos
		# else
		# 	@registers = current_user.registers.paginate(page: params[:page], per_page: 10).publicados.ultimos
		# end
		#@registers = current_user.registers.where(state:"active")
	def search
  		compare
  		render :index
	end
	def show
		#Register.where.not("activo = false")
	end
	def new
		@register = current_user.registers.new
	end
	def create
		@register = current_user.registers.new(register_params)
		if @register.save
			redirect_to @register
		else
			render :new
		end
	end
	def edit

	end
	def destroy

		@register.destroy
		redirect_to registers_path
	end
	def update
		if @register.update(register_params)
			redirect_to registers_path
		else
			render :edit
		end
	end

	def compare

		# rango1 = params[:desde]
		# rango2 = params[:hasta]
		@searchCompare = current_user.registers.search(params[:q])
		if params[:q]

			@registers = @searchCompare.result(distinct: true).order(fecha: :asc).paginate(page: params[:page], per_page: 5)
		end

	end

	private

	def set_register
		@register = Register.find(params[:id])
	end

	def register_params
		params.require(:register).permit(:monto,:medida,:fecha,:descripcion,:costos_adicionales,:category_id,:search,:consumo)
	end
end
