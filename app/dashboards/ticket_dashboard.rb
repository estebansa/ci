require "administrate/base_dashboard"

class TicketDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    tic_type: Field::BelongsTo,
    user: Field::BelongsTo,
    id: Field::Number,
    title: Field::String,
    description: Field::Text,
    status: Field::String.with_options(searchable: false),
    screenshot: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    screenshot_file_name: Field::String,
    screenshot_content_type: Field::String,
    screenshot_file_size: Field::Number,
    screenshot_updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :tic_type,
    :user,
    :id,
    :title,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :tic_type,
    :user,
    :id,
    :title,
    :description,
    :status,
    :screenshot,
    :created_at,
    :updated_at,
    :screenshot_file_name,
    :screenshot_content_type,
    :screenshot_file_size,
    :screenshot_updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :tic_type,
    :user,
    :title,
    :description,
    :status,
    :screenshot,
    :screenshot_file_name,
    :screenshot_content_type,
    :screenshot_file_size,
    :screenshot_updated_at,
  ].freeze

  # Overwrite this method to customize how tickets are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(ticket)
  #   "Ticket ##{ticket.id}"
  # end
end
