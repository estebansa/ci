require "administrate/base_dashboard"

class RegisterDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    category: Field::BelongsTo,
    id: Field::Number,
    monto: Field::Number,
    fecha: Field::DateTime,
    descripcion: Field::Text,
    medida: Field::String,
    costos_adicionales: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    state: Field::String,
    consumo: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :user,
    :category,
    :id,
    :monto,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :category,
    :id,
    :monto,
    :fecha,
    :descripcion,
    :medida,
    :costos_adicionales,
    :created_at,
    :updated_at,
    :state,
    :consumo,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :category,
    :monto,
    :fecha,
    :descripcion,
    :medida,
    :costos_adicionales,
    :state,
    :consumo,
  ].freeze

  # Overwrite this method to customize how registers are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(register)
  #   "Register ##{register.id}"
  # end
end
