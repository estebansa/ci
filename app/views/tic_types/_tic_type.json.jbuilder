json.extract! tic_type, :id, :name, :created_at, :updated_at
json.url tic_type_url(tic_type, format: :json)
