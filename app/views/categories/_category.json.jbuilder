json.extract! category, :id, :name, :color, :medida, :created_at, :updated_at
json.url category_url(category, format: :json)
