class UserMailer < ApplicationMailer

  def welcome_user(user)
    @user = user
    mail to: user.email, subject: "Sign up confirmation", from: "spendo@info.com"
  end
end
