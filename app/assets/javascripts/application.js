//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
//= require highcharts
//= require Chart.bundle
//= require chartkick
new WOW().init();

function alertsNotifications(message, type){

  if(type == 'success') {
    toastr.success(message, '¡Correcto!', {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "preventDuplicates": false,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "8000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    });
  }
  else {
    toastr.error(message, '¡Error!', {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "preventDuplicates": false,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "8000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    });
  }

}

