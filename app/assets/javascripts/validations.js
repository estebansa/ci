var email = document.getElementById('inputEmail');
email.addEventListener("keyup", function(){
  valEmail = email.value;
  if(isValidEmailAddress(valEmail)){
    email.classList.add('is-valid');
    email.classList.remove('is-invalid');
  }
  else{
   email.classList.add('is-invalid');
   email.classList.remove('is-valid');
 }
});

var pass = document.getElementById('inputPass');
pass.addEventListener("keyup", function(){
  valPass = pass.value;
  if(valPass.length >= 6){
    pass.classList.add('is-valid');
    pass.classList.remove('is-invalid');
  }
  else{
   pass.classList.add('is-invalid');
   pass.classList.remove('is-valid');
 }
});

var pass2 = document.getElementById('inputPass2');
pass2.addEventListener("keyup", function(){
  valPass2 = pass2.value;
  if(valPass === valPass2){
    pass2.classList.add('is-valid');
    pass2.classList.remove('is-invalid');
  }
  else{
   pass2.classList.add('is-invalid');
   pass2.classList.remove('is-valid');
 }
});



function categories(){

  var e = document.getElementById("selectCategories");
  var cat = e.options[e.selectedIndex].value;

  if(cat === "2"){
   med = document.getElementById("medidaRegister");
   med.value = "kW";
 }

 if(cat === "3"){
   med = document.getElementById("medidaRegister");
   med.value = "Minutos";
 }

 if(cat === "4"){
   med = document.getElementById("medidaRegister");
   med.value = "kWH";
 }
}

function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
}