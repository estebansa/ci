class Register < ApplicationRecord

	include AASM 

	belongs_to :user
	belongs_to :category

	validates :monto, presence: true
	validates :medida, presence: true
	validates :costos_adicionales, presence: true
	validates :fecha, presence: true
	validates :descripcion, presence: true, length: { minimum: 10}	
	before_create :set_costos_adicionales

	scope :activos, ->{ where(state:"active") }

	scope :ultimos, ->{ order("created_at DESC")}

	aasm column: "state" do
		state :active, initial: true
		state :unactive

		event :actived do
			transitions from: :unactive, to: :active
		end

		event :unactived do
			transitions from: :active, to: :unactive
		end

	end

	UNRANSACKABLE_ATTRIBUTES = ["id", "updated_at", "created_at", "user", "Category", "state","cantidad_medida", "user_id"]

	def self.ransackable_attributes auth_object = nil
		(column_names - UNRANSACKABLE_ATTRIBUTES) + _ransackers.keys
	end

	private

	def set_costos_adicionales
		self.costos_adicionales = 0
	end
end
