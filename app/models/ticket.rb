class Ticket < ApplicationRecord

  has_attached_file :screenshot, styles: { medium: "1280 x 720", thumb: "800x600", small: "600 x 400"}
  validates_attachment_content_type :screenshot, content_type: /\Aimage\/.*\Z/

  belongs_to :tic_type
  belongs_to :user

  validates :title, :description, :status, presence: true
  enum status: [:unsolved, :solved, :pending]
end
