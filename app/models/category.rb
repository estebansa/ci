class Category < ApplicationRecord
	validates :name, presence: true
	has_many :registers

	UNRANSACKABLE_ATTRIBUTES = ["color", "updated_at", "created_at", "medida", "id"]

	def self.ransackable_attributes auth_object = nil
		(column_names - UNRANSACKABLE_ATTRIBUTES) + _ransackers.keys
	end
end
