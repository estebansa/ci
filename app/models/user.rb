class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include PermissionsConcern

  validates :email, :email => true
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  has_many :registers
  has_many :tickets

  	UNRANSACKABLE_ATTRIBUTES = [ "user_id", "updated_at", "created_at", "medida", "id" ]

	def self.ransackable_attributes auth_object = nil
		(column_names - UNRANSACKABLE_ATTRIBUTES) + _ransackers.keys
	end

end
